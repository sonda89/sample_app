# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'developer.rikkei@gmail.com'
  layout 'mailer'
end
